package detpinit.thatchapon.lab10;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
 
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
 
class DeviceImagePanel extends JPanel {
 
    /**
     * 
     */
    private static final long serialVersionUID = -4105847750040356430L;
 
    BufferedImage img;
 
    public void paintComponent(Graphics g) {
        g.drawImage(img, 0, 0, null);
    }
 
    public DeviceImagePanel() {
        try {
            img = ImageIO.read(new File("images/galaxyNote9.jpg"));
        } catch (IOException ex) {
            ex.printStackTrace(System.err);
        }
    }
 
    public Dimension getPreferredSize() {
        if (img == null)
            return new Dimension(100, 100);
        else
            return new Dimension(img.getWidth(), img.getHeight());
    }
}
 
public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
    JPanel bottomPanel;
    DeviceImagePanel deviceImgPanel;
 
    /**
     * 
     */
    private static final long serialVersionUID = 6460143049086436859L;
 
    public MobileDeviceFormV6(String title) {
        super(title);
    }
 
    protected void initComponents() {
        super.initComponents();
        bottomPanel = new JPanel(new BorderLayout());
        deviceImgPanel = new DeviceImagePanel();
 
    }
 
    protected void addComponents() {
        super.addComponents();
        bottomPanel.add(buttonsPanel, BorderLayout.CENTER);
        bottomPanel.add(deviceImgPanel, BorderLayout.NORTH);
        overallPanel.remove(buttonsPanel);
        overallPanel.add(bottomPanel, BorderLayout.SOUTH);
        this.setContentPane(overallPanel);
    }
 
    public static void createAndShowGUI() {
        MobileDeviceFormV6 mobileDeviceForm6 = new MobileDeviceFormV6("Mobile Device Form V6");
        mobileDeviceForm6.addComponents();
        mobileDeviceForm6.addMenus();
        mobileDeviceForm6.setFrameFeatures();
    }
 
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}