package detpinit.thatchapon.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.Box;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MobileDeviceFormV7 extends MobileDeviceFormV6 implements ActionListener {

	public MobileDeviceFormV7(String title) {
		super(title);
	}
	public void handleOKButton() {
		if(iOS.isSelected()) {
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + bNameTxtField.getText() + ", Model Name: " + mNameTxtField.getText()
					+ ", Weight: " + weightTxtField.getText() + ", Price: " + priceTxtField.getText() + 
					"\n" + " OS: " + iOS.getText()+ "\n Type: " + typeBox.getSelectedItem()
					+ "\n Features: " + features.getSelectedValuesList() + "\n Review: " + reviewTxtArea.getText());
		}
		else if(android.isSelected()){	
			JOptionPane.showMessageDialog(null,
					"Brand Name: " + bNameTxtField.getText() + ", Model Name: " + mNameTxtField.getText()
					+ ", Weight: " + weightTxtField.getText() + ", Price: " + priceTxtField.getText() + 
					"\n" + " OS: " + android.getText() + "\n Type: " + typeBox.getSelectedItem()
					+ "\n Features: " + features.getSelectedValuesList() + "\n Review: " + reviewTxtArea.getText());
		}
	}
	public void handleCancelButton() {
		bNameTxtField.setText(" ");
		mNameTxtField.setText(" ");
		weightTxtField.setText(" ");
		priceTxtField.setText(" ");
		reviewTxtArea.setText(" ");	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object src = e.getSource();
		if (src == okButton) {
			handleOKButton();
			
		} else if (src == cancelButton) {
			handleCancelButton();
		}
	}
	

	protected void addListeners() {
		okButton.addActionListener(this);
		cancelButton.addActionListener(this);
		
		iOS.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(iOS.isSelected()) {
					JOptionPane.showMessageDialog(null, "Your os platform is now changed to iOS");
				}
			}
		});
		
		android.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(android.isSelected()) {
					JOptionPane.showMessageDialog(null, "Your os platform is now changed to Android OS");

				}
			}
		});
				
		features.addListSelectionListener(new ListSelectionListener(){
		      public void valueChanged(ListSelectionEvent event) {
		          if (event.getValueIsAdjusting())
		            return;
		          JOptionPane.showMessageDialog(null, features.getSelectedValuesList());
		        }
		      });
		
		typeBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (e.getStateChange() == ItemEvent.SELECTED) {
			          JOptionPane.showMessageDialog(null, "Type is updated to " + typeBox.getSelectedItem());
				}
			}
		});
	}
	public void addComponents() {
		super.addComponents();
		features.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		features.setSelectedIndex(3);
		features.addSelectionInterval(0, 1);
		
		android.setSelected(true);
	}
	
	public void addMenus() {
		super.addMenus();
	}

	

	public static void createAndShowGUI() {
		MobileDeviceFormV7 MobileDeviceFormV7 = new MobileDeviceFormV7("Mobile Device Form V7");
		MobileDeviceFormV7.addComponents();
		MobileDeviceFormV7.addMenus();
		MobileDeviceFormV7.setFrameFeatures();
		MobileDeviceFormV7.addListeners();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}