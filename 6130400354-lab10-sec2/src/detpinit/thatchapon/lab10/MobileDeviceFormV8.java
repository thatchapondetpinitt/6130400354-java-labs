package detpinit.thatchapon.lab10;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7  {
	protected JMenu superColorMenuForSubmenu;
	JMenuItem redmenu = new JMenuItem("Red");
	JMenuItem greenmenu = new JMenuItem("Green");
	JMenuItem bluemenu = new JMenuItem("Blue");
	JMenuItem customMI = new JMenuItem("Custom ...");

	public MobileDeviceFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	protected void addSubMenus() { 
		super.addSubMenus();
		customMI = new JMenuItem("Custom ...");
		colorMenu.add(customMI);
		superColorMenuForSubmenu = new JMenu("Color");
		configMenu.remove(colorMenu);
		configMenu.remove(sizeMenu);
		superColorMenuForSubmenu.add(redmenu);
		superColorMenuForSubmenu.add(greenmenu);
		superColorMenuForSubmenu.add(bluemenu);
		superColorMenuForSubmenu.add(customMI);
		configMenu.add(superColorMenuForSubmenu);
		configMenu.add(sizeMenu);

		fileMenu.setMnemonic(KeyEvent.VK_F);
		configMenu.setMnemonic(KeyEvent.VK_C);
		newMI.setMnemonic(KeyEvent.VK_N);
		openMI.setMnemonic(KeyEvent.VK_O);
		saveMI.setMnemonic(KeyEvent.VK_S);
		exitMI.setMnemonic(KeyEvent.VK_X);
		superColorMenuForSubmenu.setMnemonic(KeyEvent.VK_L);
		redmenu.setMnemonic(KeyEvent.VK_R);
		greenmenu.setMnemonic(KeyEvent.VK_G);
		bluemenu.setMnemonic(KeyEvent.VK_B);
		customMI.setMnemonic(KeyEvent.VK_U);

		newMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
		openMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
		saveMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK));
		exitMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_MASK));
		redmenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_MASK));
		greenmenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, KeyEvent.CTRL_MASK));
		bluemenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.CTRL_MASK));
		customMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, KeyEvent.CTRL_MASK));
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV8 MobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");
		MobileDeviceFormV8.addComponents();
		MobileDeviceFormV8.addMenus();
		MobileDeviceFormV8.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	

}
