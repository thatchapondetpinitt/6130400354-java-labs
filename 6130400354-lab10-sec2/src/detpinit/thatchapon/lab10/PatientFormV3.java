package detpinit.thatchapon.lab10;

import java.awt.BorderLayout;
import java.awt.GridLayout;
 
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
 
public class PatientFormV3 extends MobileDeviceFormV2 {
    /**
     * 
     */
    private static final long serialVersionUID = -3170036318568067861L;
    protected JLabel typeLabel;
    protected JComboBox<String> typeList;
    protected JPanel typePanel;
    protected JMenuBar menuBar;
    protected JMenu fileMenu, editMenu, configMenu;
    protected JMenuItem newMI, openMI, saveMI, exitMI;
    protected JMenuItem colorMI, sizeMI;
    public PatientFormV3(String title) {
        super(title);
    }
     
    protected void initComponents() {
        super.initComponents();
        typeLabel = new JLabel("Type:");
        typeList = new JComboBox<String>();
        typeList.addItem("Inpatient");
        typeList.addItem("Outpatient");
        typeList.setSelectedItem("Outpatient");
        typePanel = new JPanel(new GridLayout(1,2));
    }
     
    protected void addComponents() {
        super.addComponents();
        typePanel.add(typeLabel);
        typePanel.add(typeList);
        overallPanel.add(typePanel, BorderLayout.CENTER);
    }
     
    protected void initMenus() {
        menuBar = new JMenuBar();
        fileMenu = new JMenu("File");
        configMenu = new JMenu("Config");
        newMI = new JMenuItem("New");
        openMI = new JMenuItem("Open");
        saveMI = new JMenuItem("Save");
        exitMI = new JMenuItem("Exit");
        colorMI = new JMenuItem("Color");
        sizeMI = new JMenuItem("Size");
    }
     
    protected void addMenus() {
        initMenus();
        fileMenu.add(newMI);
        fileMenu.add(openMI);
        fileMenu.add(saveMI);
        fileMenu.add(exitMI);
        configMenu.add(colorMI);
        configMenu.add(sizeMI);
        menuBar.add(fileMenu);
        menuBar.add(configMenu);
        setJMenuBar(menuBar);
    }
    public static void createAndShowGUI(){
        PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
        patientForm3.addComponents();
        patientForm3.addMenus();
        patientForm3.setFrameFeatures();
    }
     
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}