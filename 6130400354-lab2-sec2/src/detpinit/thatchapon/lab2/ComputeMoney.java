package detpinit.thatchapon.lab2;

public class ComputeMoney {

	public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println("ComputeMoney <1,000Baht> <500Baht> <100Baht> <20Baht>");
			System.exit(0);
		}
		double onezero = 1000 * Double.parseDouble(args[0]);
		double fivezero = 500 * Double.parseDouble(args[1]);
		double one = 100 * Double.parseDouble(args[2]);
		double two = 20 * Double.parseDouble(args[3]);
		double sum = onezero + fivezero + one+ two;
		System.out.println("Total Money is "+sum);

	}

}
