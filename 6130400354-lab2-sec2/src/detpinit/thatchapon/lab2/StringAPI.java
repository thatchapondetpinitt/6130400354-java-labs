package detpinit.thatchapon.lab2;

public class StringAPI {

	public static void main(String[] args) {		
		String schoolName = args[0];
		if (schoolName.endsWith("University")) {
			System.out.println(schoolName+" is a university");
		} else if (schoolName.endsWith("College")) {
			System.out.println(schoolName+" is a college");
		} else {
			System.out.println(schoolName+" is a neither a university nor a college");
		}
		
	
	}
}
