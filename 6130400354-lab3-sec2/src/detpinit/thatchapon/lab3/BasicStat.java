package detpinit.thatchapon.lab3;

import java.util.Arrays;

public class BasicStat {
	static double[] number;
	static int input;

	public static void main(String[] args) {
		acceptInput(args);
		displayStats();
	}
	
	public static void acceptInput(String[] args) {
		input = Integer.parseInt(args[0]);
		double[] array = new double[input];
		if (input != args.length - 1) {
			System.err.println("<BasicStat> <numNumbers> <numbers>...");
			System.exit(0);
		}
		for (int i = 1; i < args.length; i++) {
			array[i - 1] = Double.parseDouble(args[i]);
		}
		number = array;
	}
		public static void displayStats() {
		double[] array = new double[input];
		double numberInput = input;
		double sum, Calculate, n, deviation;
		array = number;
		Arrays.sort(array);
		System.out.println("Max is " + array[array.length - 1] + " Min is " + array[0]);
		
		sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		n = (1 / numberInput) * sum; 
		Calculate = 0;
		for (int i = 0; i < array.length; i++) {
			Calculate += Math.pow((array[i] - n), 2);
		}
		deviation = Math.sqrt((1 / numberInput) * Calculate);
		System.out.println("Average is " + (sum / numberInput));
		System.out.println("Standard Deviation is " + deviation);
	}

}
