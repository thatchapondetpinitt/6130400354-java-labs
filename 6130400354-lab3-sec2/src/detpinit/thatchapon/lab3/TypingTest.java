package detpinit.thatchapon.lab3;

import java.util.*; 

public class TypingTest {
	
	public static final double STCerrent = System.currentTimeMillis();
	
	public static void Time_Second() {
		double Answer = System.currentTimeMillis();
		double ans = (Answer - STCerrent) / 1000;
		System.out.println("Your time is " + ans);		
		if (ans <= 12) {
			System.out.println("You type faster than average person");
		} else {
			System.out.println("You type slower than average person");
		}
		System.exit(0);
	}
	public static void main(String[] args) {
		String[] color = {"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET"};
		StringBuilder s1 = new StringBuilder("");
		for (int i = 0; i < 8; i++) {
			String col = (color[new Random().nextInt(color.length)]);
			s1.append(col);
			s1.append(" ");
		}
		String sb1n = s1.substring(0, s1.length()-1);
		System.out.println(sb1n);
		while(true) {
			System.out.print("Type your answer: ");
			Scanner scan = new Scanner(System.in);
			String ans = scan.nextLine().toUpperCase();
			if (ans.equals(sb1n)) {
				Time_Second();
				scan.close();
			} else {
				continue;
			}	
		
		}
	}

}
