package detpinit.thatchapon.lab4;

public class HondaAuto {

	private int gasoline, speed, maxSpeed, acceleration;
	private String model;
	
	public HondaAuto(int maxSpeed, int acceleration, String model) {
		this.maxSpeed = maxSpeed;
		this.acceleration = acceleration;
		this.model = model;
		this.gasoline = 100;
	}

	public int refuel(){
		this.gasoline = 100;
		System.out.println("refuels");
		return gasoline;
	}

	public int accelerate() {
		if (speed > maxSpeed) {
			this.speed = maxSpeed;
		}
		else {
			this.speed += acceleration;
		}
		this.gasoline -= 10;
		System.out.println(model + " accelerates");
		return speed;
	}
	
	public int brake(){
		if (speed <= 0) {
			this.speed = 0;
		}
		else {
			this.speed -= acceleration;
		}
		this.gasoline -= 10;
		System.out.println(model + " brakes");
		return speed;
	}

	public void setSpeed(int speed) {
		if (speed < 0)
			this.speed = 0;
		else if (speed > maxSpeed) {
			this.speed = maxSpeed;
		}
		else {
			this.speed = speed;
		}
	}
	
	public int getSpeed() {
		return speed;
	}
	
	public int getGasoline() {
		return gasoline;
	}
	

	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}
	

	public int getMaxSpeed() {
		return maxSpeed;
	}
	

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	

	public int getAcceleration() {
		return acceleration;
	}
	

	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}
	

	public String getModel() {
		return model;
	}
	

	public void setModel(String model) {
		this.model = model;
	}
	
	@Override
	public String toString() {
		return model + " gasoline:" + gasoline + " speed:" + speed + " maxSpeed:" + maxSpeed
				+ " acceleration:" + acceleration;
	}
}
