package detpinit.thatchapon.lab4;

public class TestDrive2 {

	private static void isFaster(ToyotaAuto car1, HondaAuto car2) {
		if (car1.getMaxSpeed() > car2.getMaxSpeed()) {
			System.out.println(car1.getModel() + " is faster than " + car2.getModel());
		}
		else if (car1.getMaxSpeed() < car2.getMaxSpeed()) {
			System.out.println(car1.getModel() + " is NOT faster than " + car2.getModel());
		}
		else {
			System.out.println(car1.getModel() + " is equally fast " + car2.getModel());
		}
	}

	private static void isFaster(HondaAuto car2, ToyotaAuto car1) {
		if (car2.getMaxSpeed() > car1.getMaxSpeed()) {
			System.out.println(car1.getModel() + " is faster than " + car2.getModel());
		}
		else if (car2.getMaxSpeed() < car1.getMaxSpeed()) {
			System.out.println(car1.getModel() + " is NOT faster than " + car2.getModel());
		}
		else {
			System.out.println(car2.getModel() + " is equally fast " + car1.getModel());
		}
	}
	
	public static void main(String[] args) {
		ToyotaAuto car1 = new ToyotaAuto(200, 10, "Vios");
		HondaAuto car2 = new HondaAuto(220, 8, "City");
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.accelerate();
		car2.accelerate();
		car2.accelerate();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.brake();
		car1.brake();
		car2.brake();
		
		System.out.println(car1);
		System.out.println(car2);
		
		car1.refuel();
		car2.refuel();
		
		System.out.println(car1);
		System.out.println(car2);
		
		isFaster(car1, car2);
		isFaster(car2, car1);
	}
}
