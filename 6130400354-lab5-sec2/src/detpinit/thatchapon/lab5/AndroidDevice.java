package detpinit.thatchapon.lab5;

abstract public class AndroidDevice {
	private static String os = "Android";

	abstract public void usage();

	public static String os() {
		return os;
	}
}