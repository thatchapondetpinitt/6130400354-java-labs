package detpinit.thatchapon.lab5;


public class AndroidSmartWatch extends AndroidDevice {
	private String modelName;
	private String brandName;
	private int price;

	public void displayTime() {
		System.out.println("Display time only using a digital format");
	}
	
	public AndroidSmartWatch(String brandName, String modelName, int price) {
		this.modelName = modelName;
		this.brandName = brandName;
		this.price = price;
	}
	public String ModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String BrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public int Price() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public void usage() {
		System.out.println("AndroidSmartWatch Usage: Show time, date, your heart rate, and your step count");
	}

	@Override
	public String toString() {
		return "AndroidSmartWatch[Brand name:" + BrandName() + ", Model name: " + ModelName() + ", Price:"
				+ Price() + " Baht]";
	}
}
