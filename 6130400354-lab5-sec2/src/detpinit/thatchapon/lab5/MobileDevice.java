package detpinit.thatchapon.lab5;

public class MobileDevice {
	private String modelName;
	private String os;
	private int price;
	private int weight;
	public static void main(String[] args) {		
	}	
	public MobileDevice(String modelName, String os, int price, float weight) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = (int)weight;
	}
	public MobileDevice(String modelName, String os, int price) {
		this.modelName = modelName;
		this.os = os;
		this.price = price;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "MobileDevice [model Name:" + modelName + ", OS:" + os + ", price:" + price + ", weight:" + weight + " g" +"]";
	}
	
}
