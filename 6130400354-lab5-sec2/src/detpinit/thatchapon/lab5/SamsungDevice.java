package detpinit.thatchapon.lab5;

public class SamsungDevice extends MobileDevice {
	private static String brand = "Samsung";
	private double androidversion;
	
	public double getAndroidversion() {
		return androidversion;
	}
	public void setAndroidversion(double version) {
		this.androidversion = version;
	}
	public static String getBrand() {
		return brand;
	}
	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}
	public SamsungDevice(String modelName,int price,double androidversion) {
		super(modelName,"Android",price);
		this.androidversion = androidversion;	
	}
	
	public SamsungDevice(String modelName,int price,float weight,double androidversion) {
		super(modelName,"Android",price, weight);
		this.androidversion = androidversion;		
	}
	
	@Override
	public String toString() {
		return "SamsungDevice [Model Name:"+super.getModelName()
		+", OS:"+super.getOs()+ ", Price:"+super.getPrice()+" Baht,weight"
		+super.getWeight()+" g, Android version:"+getAndroidversion()+"]";
	}		
}

