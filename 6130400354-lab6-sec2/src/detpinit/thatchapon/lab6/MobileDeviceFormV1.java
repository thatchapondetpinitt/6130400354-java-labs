package detpinit.thatchapon.lab6;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

public class MobileDeviceFormV1 extends MySimpleWindow {
	public MobileDeviceFormV1(String title) {
		super(title);
	}
	protected JLabel nameLabel;
	protected JLabel modelLabel;
	protected JLabel weightLabel;
	protected JLabel priceLabel;
	protected JLabel mobileLabel;

	protected JTextField nameTextField;
	protected JTextField modelTextField;
	protected JTextField weightTextField;
	protected JTextField priceTextField;

	protected JRadioButton androidRadio;
	protected JRadioButton osRadio;
	protected JPanel radioButtonPanel;
	protected ButtonGroup groupBotton;

	protected void addComponents() {
		super.addComponents();
		contentPanel.setLayout(new GridLayout(0, 2));
		radioButtonPanel = new JPanel();

		nameLabel = new JLabel("Brand Name: ");
		modelLabel = new JLabel("Model Name: ");
		weightLabel = new JLabel("Weight (kg.): ");
		priceLabel = new JLabel("Price (Baht): ");
		mobileLabel = new JLabel("Mobile OS: ");

		nameTextField = new JTextField();
		modelTextField = new JTextField();
		weightTextField = new JTextField();
		priceTextField = new JTextField();
		groupBotton = new ButtonGroup();
		androidRadio = new JRadioButton("Android");
		osRadio = new JRadioButton("iOS");
		groupBotton.add(androidRadio);
		groupBotton.add(osRadio);

		contentPanel.add(nameLabel);
		contentPanel.add(nameTextField);
		contentPanel.add(modelLabel);
		contentPanel.add(modelTextField);
		contentPanel.add(weightLabel);
		contentPanel.add(weightTextField);
		contentPanel.add(priceLabel);
		contentPanel.add(priceTextField);
		contentPanel.add(mobileLabel);

		radioButtonPanel.add(androidRadio);
		radioButtonPanel.add(osRadio);
		contentPanel.add(radioButtonPanel);

	}
	protected void setFrameFutures() {
		super.setFrameFeatures();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV1 mobileDeviceFormV1 = new MobileDeviceFormV1("Mobile Device Form V1");
		mobileDeviceFormV1.addComponents();

		mobileDeviceFormV1.setFrameFeatures();
		mobileDeviceFormV1.setVisible(true);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}
}	