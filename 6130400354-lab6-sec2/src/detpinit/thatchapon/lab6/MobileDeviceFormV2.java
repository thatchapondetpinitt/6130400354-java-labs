package detpinit.thatchapon.lab6;
import java.awt.*;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

public class MobileDeviceFormV2 extends MobileDeviceFormV1 {
	public MobileDeviceFormV2(String title) {
		super(title);
	}

	protected JLabel typeLabel;
	protected JComboBox typeComboBox;
	protected JLabel reviewLabel;
	protected JPanel reviewPanel;
	protected JTextArea textArea;
	protected JScrollPane scrollPane;
	String[] typeComboBoxList = { "Phone", "Tablet", "Smart TV" };

	protected void addComponents() {
		super.addComponents();
		contentPanel.setLayout(new GridLayout(0, 2));
		typeLabel = new JLabel("Type: ");
		reviewLabel = new JLabel("Review: ");
		typeComboBox = new JComboBox(typeComboBoxList);
		reviewPanel = new JPanel();
		reviewPanel.setLayout(new BorderLayout());
		textArea = new JTextArea("Bigger than previous Note phones in every way, "
				+ "the Samsung Galaxy Note 9 has a larger 6.4-inch screen, "
				+ "heftier 4,000mAh battery, and a massive 1TB of storage option. ", 2, 35);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane = new JScrollPane(textArea);
		reviewPanel.add(reviewLabel, BorderLayout.NORTH);
		reviewPanel.add(scrollPane, BorderLayout.SOUTH);
		contentPanel.add(typeLabel);
		contentPanel.add(typeComboBox);
		centerPanel.add(reviewPanel, BorderLayout.CENTER);	
		contentPanel.setBorder(new EmptyBorder(0, 0, 10, 0));

	}
	protected void setFrameFutures() {
		super.setFrameFeatures();
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV2 mobileDeviceFormV2 = new MobileDeviceFormV2("Mobile Device Form V2");
		mobileDeviceFormV2.addComponents();
		mobileDeviceFormV2.setFrameFeatures();
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}