package detpinit.thatchapon.lab6;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV3 extends MobileDeviceFormV2 {
	protected JMenuBar menuBar;
	protected JMenu menuFile;
	protected JMenu configFile;
	protected JMenuItem newItem;
	protected JMenuItem openItem;
	protected JMenuItem saveItem;
	protected JMenuItem exitItem;
	protected JMenuItem colorItem;
	protected JMenuItem sizeItem;

	protected JLabel featuresLabel;
	protected JList list;
	protected JPanel listPanel;
	protected JLabel listLabel;

	public MobileDeviceFormV3(String title) {
		super(title);	
	}

	protected void addComponents() {
		super.addComponents();

		listLabel = new JLabel("Features: ");

		String listFeature[] = { "Design and build quality", "Great Camera", "Screen", "Battery Life" };
		list = new JList(listFeature);

		listPanel = new JPanel();
		listPanel.setLayout(new GridLayout(0,2 ));
		listPanel.add(listLabel);
		listPanel.add(list);
		
		centerPanel.add(listPanel, BorderLayout.NORTH);

	}

	protected void addMenu() {
		menuBar = new JMenuBar();
		menuFile = new JMenu("File");
		configFile = new JMenu("Config");

		newItem = new JMenuItem("New");
		openItem = new JMenuItem("Open");
		saveItem = new JMenuItem("Save");
		exitItem = new JMenuItem("Exit");
		colorItem = new JMenuItem("Color");
		sizeItem = new JMenuItem("Size");
		menuBar.add(menuFile);
		menuBar.add(configFile);
		menuFile.add(newItem);
		menuFile.add(openItem);
		menuFile.add(saveItem);
		menuFile.add(exitItem);
		configFile.add(colorItem);
		configFile.add(sizeItem);

		this.setJMenuBar(menuBar);
	}
	public static void createAndShowGUI() {
		MobileDeviceFormV3 mobileDeviceFormV3 = new MobileDeviceFormV3("Mobile Device Form V3");
		mobileDeviceFormV3.addComponents();
		mobileDeviceFormV3.addMenu();
		mobileDeviceFormV3.setFrameFeatures();
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}