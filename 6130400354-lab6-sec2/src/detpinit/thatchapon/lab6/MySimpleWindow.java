package detpinit.thatchapon.lab6;

import java.awt.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MySimpleWindow extends JFrame {
	protected JButton cancelButton;
	protected JButton okButton;
	protected JPanel contentPanel;
	protected JPanel centerPanel;
	protected JPanel windowPanel;
	protected JPanel bottonPanel;
	
	
	public MySimpleWindow(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MySimpleWindow mySimple = new MySimpleWindow("My Simple Window");
		mySimple.addComponents();
		
		mySimple.setFrameFeatures();
		mySimple.setVisible(true);

	}
	protected void setFrameFeatures() {
		this.setLocationRelativeTo(null);
		pack();
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	protected void addComponents() {
		okButton = new JButton("OK");
		bottonPanel = new JPanel();
		
		cancelButton = new JButton("Cancel");
		
		contentPanel = new JPanel();
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());

		bottonPanel.add(cancelButton);
		bottonPanel.add(okButton);

		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(contentPanel, BorderLayout.NORTH);
		windowPanel.add(centerPanel, BorderLayout.CENTER);
		windowPanel.add(bottonPanel, BorderLayout.SOUTH);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}