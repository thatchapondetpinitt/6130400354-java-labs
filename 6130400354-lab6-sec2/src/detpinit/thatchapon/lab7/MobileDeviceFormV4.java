package detpinit.thatchapon.lab7;

import detpinit.thatchapon.lab6.MobileDeviceFormV3;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;


public class MobileDeviceFormV4 extends MobileDeviceFormV3{
	protected JMenuItem redColor;
	protected JMenuItem greenColor;
	protected JMenuItem blueColor;
	protected JMenuItem sizeMenu16;
	protected JMenuItem sizeMenu20;
	protected JMenuItem sizeMenu24;
	
	
public MobileDeviceFormV4(String title) {
	super(title);	
			
}
protected void updateMenuIcon() {
	newItem.setIcon(new ImageIcon("Image/new.jpg"));
}
protected void addMenu() {
	super.addMenu();
	updateMenuIcon();
	addSubMenu();
	
	}
protected void addSubMenu() {
	menuBar = new JMenuBar();
	configFile = new JMenu("Config");
	redColor = new JMenuItem("Red");
	greenColor = new JMenuItem("Green");
	blueColor = new JMenuItem("Blue");

	sizeMenu16 = new JMenuItem("16");
	sizeMenu20 = new JMenuItem("20");
	sizeMenu24 = new JMenuItem("24");
	
	colorItem.add(redColor);
	colorItem.add(greenColor);
	colorItem.add(blueColor);

	sizeItem.add(sizeMenu16);
	sizeItem.add(sizeMenu20);
	sizeItem.add(sizeMenu24);
}
public static void main(String[]args) {
	SwingUtilities.invokeLater(new Runnable()  {
		public void run() {
			createAndShowGUI();
		}
	});
}
public static void createAndShowGUI() {
	MobileDeviceFormV4 mobileDeviceForm4 = new MobileDeviceFormV4("Mobile Device Form V4");
	mobileDeviceForm4.addComponents();
	mobileDeviceForm4.addMenu();
	mobileDeviceForm4.setFrameFeatures();

	}
}