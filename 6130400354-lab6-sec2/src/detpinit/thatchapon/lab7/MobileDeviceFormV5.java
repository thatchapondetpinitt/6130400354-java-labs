package detpinit.thatchapon.lab7;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingUtilities;
	
	public class MobileDeviceFormV5 extends MobileDeviceFormV4 {

			public MobileDeviceFormV5(String title) {
				super(title);
				
			}

			public static void main(String[] args) {
				
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						createAndShowGUI();
					}
				});
			}
			
			public static void createAndShowGUI() {
				
				MobileDeviceFormV5 mobileDeviceFormV5 = new MobileDeviceFormV5("Mobile Device Form V5");
				mobileDeviceFormV5.addComponents();
				mobileDeviceFormV5.addMenu();
				mobileDeviceFormV5.initComponents();
				mobileDeviceFormV5.setFrameFeatures();
				mobileDeviceFormV5.setVisible(true);
			}
			@Override
			protected void addComponents() {
				super.addComponents();
				okButton.setForeground(Color.BLUE);
				cancelButton.setForeground(Color.RED);
			}

			protected void initComponents() {
				
				nameLabel.setFont(new Font("Serif", Font.PLAIN,14));
				modelLabel.setFont(new Font("Serif", Font.PLAIN,14));
				weightLabel.setFont(new Font("Serif", Font.PLAIN,14));
				priceLabel.setFont(new Font("Serif", Font.PLAIN,14));
				mobileLabel.setFont(new Font("Serif", Font.PLAIN,14));
				typeLabel.setFont(new Font("Serif", Font.PLAIN,14));
				reviewLabel.setFont(new Font("Serif", Font.PLAIN,14));
				listLabel.setFont(new Font("Serif", Font.PLAIN,14));
				
				nameTextField.setFont(new Font("Serif", Font.BOLD,14));
				modelTextField.setFont(new Font("Serif", Font.BOLD,14));
				weightTextField.setFont(new Font("Serif", Font.BOLD,14));
				priceTextField.setFont(new Font("Serif", Font.BOLD,14));
				textArea.setFont(new Font("Serif", Font.BOLD,14));
			}
			
	}
