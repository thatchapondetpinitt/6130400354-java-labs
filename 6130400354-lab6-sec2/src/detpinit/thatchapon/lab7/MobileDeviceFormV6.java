package detpinit.thatchapon.lab7;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MobileDeviceFormV6 extends MobileDeviceFormV5 {
	protected JPanel imagesPanel;

	public MobileDeviceFormV6(String title) {
		super(title);
		
	}

	@Override 
	protected void addComponents() {
		super.addComponents();
		ReadImage imageShow = new ReadImage();

		imagesPanel = new JPanel();
		reviewPanel.add(imageShow, BorderLayout.SOUTH);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV6 mobileDeviceFormV6 = new MobileDeviceFormV6("Mobile Device Form V6");
		mobileDeviceFormV6.addComponents();
		mobileDeviceFormV6.addMenu();
		mobileDeviceFormV6.initComponents();
		mobileDeviceFormV6.setFrameFeatures();
	}
}
