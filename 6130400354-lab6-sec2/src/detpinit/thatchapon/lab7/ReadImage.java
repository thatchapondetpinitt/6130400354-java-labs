package detpinit.thatchapon.lab7;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

		import javax.imageio.ImageIO;
		import javax.swing.JPanel;

		public class ReadImage extends JPanel {
			BufferedImage img;
			String filename = "Image/galaxyNote9.jpg";

			@Override
			protected void paintComponent(Graphics g) {
				g.drawImage(img, 0, 0, null);
			}
			protected ReadImage() {
				try {
					img = ImageIO.read(new File(filename));
				} catch (IOException e) {
					e.printStackTrace(System.err);
				}
			}

			@Override
			public Dimension getPreferredSize() {
				if (img == null)
					return new Dimension(100, 100);
				else
					return new Dimension(img.getWidth(), img.getHeight());
			}
	
	}


