package detpinit.thatchapon.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;

	public MyCanvas() {
		super();
		setPreferredSize();
		setBackground(Color.black);
	}

	public void setPreferredSize() {
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setColor(Color.white);
		g2d.drawOval((getWidth()-300)/2,(getHeight()-300)/2,300,300);
		g2d.fillOval((getWidth()-95)/2,(getHeight()-150)/2,30,60);
		g2d.fillOval((getWidth()+50)/2,(getHeight()-150)/2,30,60);
		g2d.fillRect((getWidth()-85)/2,(getHeight()+130)/2,100,10);
	}
}