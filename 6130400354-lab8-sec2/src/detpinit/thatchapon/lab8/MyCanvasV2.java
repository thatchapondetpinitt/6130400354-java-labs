package detpinit.thatchapon.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;

public class MyCanvasV2 extends MyCanvas {
	protected MyBall ball;
	protected MyPedal pedal;
	protected MyBrick brick;

	public MyCanvasV2() {
		super();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);

		g2d.setColor(Color.WHITE);
		Shape ball = new MyBall((WIDTH / 2) - 15, (HEIGHT / 2) - 15);
		g2d.fill(ball);
		MyPedal pedal = new MyPedal((WIDTH / 2) - 50, HEIGHT - 10);
		g2d.fill(pedal);
		MyBrick brick = new MyBrick((WIDTH / 2) - 40, 0);
		g2d.fill(brick);

		g2d.drawLine(WIDTH / 2, 0, WIDTH / 2, HEIGHT);
		g2d.drawLine(0, HEIGHT / 2, WIDTH, HEIGHT / 2);
	}
}