package detpinit.thatchapon.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV3 extends MyCanvasV2 {
	public MyCanvasV3() {
		super();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);

		ball = new MyBall(0, 0);
		int width = WIDTH - 30;
		int height = HEIGHT - 30;
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				ball.x = 0 + i * width;
				ball.y = 0 + j * height;
				g2d.setColor(Color.WHITE);
				g2d.fill(ball);
			}
		}

		int width1 = 80;
		brick = new MyBrick((WIDTH / 2) - 400, (HEIGHT / 2) - 10);
		for (int i = 0; i < 10; i++) {
			brick.x = 0 + i * width1;
			g2d.setColor(Color.WHITE);
			g2d.fill(brick);
			g2d.setStroke(new BasicStroke(5));
			g2d.setColor(Color.BLACK);
			g2d.draw(brick);

		}

	}
}