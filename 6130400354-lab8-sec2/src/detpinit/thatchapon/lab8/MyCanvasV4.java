package detpinit.thatchapon.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class MyCanvasV4 extends MyCanvasV3 {

	public MyCanvasV4() {
		super();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fillRect(0, 0, WIDTH, HEIGHT);

		int width = 80;
		int height = -20;

		Color[] color = { Color.PINK, Color.BLUE, Color.ORANGE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.RED };

		brick = new MyBrick((WIDTH / 2) - 400, (HEIGHT / 2) - 10);
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 7; j++) {
				brick.x = 0 + i * width;
				brick.y = (HEIGHT / 3) + j * height;
				g2d.setColor(color[j]);
				g2d.fill(brick);
				g2d.setStroke(new BasicStroke(5));
				g2d.setColor(Color.BLACK);
				g2d.draw(brick);
			}
		}

		pedal = new MyPedal((WIDTH / 2) - 50, HEIGHT - 10);
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
		ball = new MyBall((WIDTH / 2) - 15, HEIGHT - 40);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

	}
}