package detpinit.thatchapon.lab8;
import javax.swing.*;

public class MyFrame extends JFrame {
	public MyFrame(String string) {
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				createAndShowGUI();
			}		
		});
	}
	private static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();

	}
	protected void addComponents() {
		add(new MyCanvas());
	}
	public void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}