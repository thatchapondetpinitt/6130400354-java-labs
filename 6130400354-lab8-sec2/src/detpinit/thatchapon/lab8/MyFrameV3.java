package detpinit.thatchapon.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

public class MyFrameV3 extends MyFrameV2 {
	public MyFrameV3(String text) {
		super(text);
	}
	public static void createAndShowGUI() {
		MyFrameV3 msw = new MyFrameV3("My Frame V3");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	
	}
	protected void addComponents() {
		add(new MyCanvasV3());
	}

}

