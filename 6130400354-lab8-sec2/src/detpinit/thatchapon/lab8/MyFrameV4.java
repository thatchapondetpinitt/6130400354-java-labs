package detpinit.thatchapon.lab8;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.SwingUtilities;

public class MyFrameV4 extends MyFrameV3 {
	public MyFrameV4(String text) {
		super(text);
	}

	public static void createAndShowGUI() {
		MyFrameV4 msw = new MyFrameV4("My Frame V4");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		add(new MyCanvasV4());
	}

}