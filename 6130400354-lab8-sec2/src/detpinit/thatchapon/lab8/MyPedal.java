package detpinit.thatchapon.lab8;

import java.awt.geom.Rectangle2D;

public class MyPedal extends Rectangle2D.Double {
	public final static int PedalWidth = 100;
	public final static int PedalHeight = 10;

	public MyPedal(int x, int y) {
		super(x, y, PedalWidth, PedalHeight);
	}
}