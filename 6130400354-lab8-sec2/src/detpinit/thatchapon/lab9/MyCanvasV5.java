package detpinit.thatchapon.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import detpinit.thatchapon.lab8.MyBall;
import detpinit.thatchapon.lab8.MyCanvas;
import detpinit.thatchapon.lab8.MyCanvasV4;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable {
	protected MyBallV2 ball = new MyBallV2(0, MyCanvas.HEIGHT / 2 - MyBall.DIAMETER / 2);
	protected Thread run = new Thread(this);

	public MyCanvasV5() {
		super();
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		run.start();
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
	}

	@Override
	public void run() {
		while (true) {
			// Stop the ball when hit the right side of the wall
			if (ball.x + MyBall.DIAMETER > MyCanvas.WIDTH) {
				break;
			}
			// move the ball
			ball.move();
			repaint();
			// Delay
			try {
				Thread.sleep(10);
			} catch (InterruptedException ex) {

			}
		}
	}
}