package detpinit.thatchapon.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import detpinit.thatchapon.lab8.MyBall;
import detpinit.thatchapon.lab8.MyCanvas;

public class MyCanvasV6 extends MyCanvasV5 implements Runnable {
	protected MyBallV2 ball2 = new MyBallV2(WIDTH/2, HEIGHT/2);
	protected Thread running2 = new Thread(this);
	public MyCanvasV6() {
		super();
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running2.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		//Draw a ball
		g2d.setColor(Color.white);
		g2d.fill(ball2);
		
	}


	public void run() {
		while (true) {
			// bounce the ball when hit the right or the left side of the wall
			if (ball2.x + MyBall.DIAMETER >= MyCanvas.WIDTH) {
				ball2.x = MyCanvas.WIDTH - MyBall.DIAMETER;
				ball2.ballVelX *= -1;
			} else if (ball2.x + MyBall.DIAMETER <= 30) {
				ball2.x = 0;
				ball2.ballVelX *= -1;
			}

			// bounce the ball when hit the top or the bottom of the wall
			else if (ball2.y + MyBall.DIAMETER >= MyCanvas.HEIGHT) {
				ball2.y = MyCanvas.HEIGHT - MyBall.DIAMETER;
				ball2.ballVelY *= -1;
			} else if (ball.y + MyBall.DIAMETER <= 30) {
				ball2.y = 0;
				ball2.ballVelY *= -1;
			
			//move the ball
			ball2.move();
			
			repaint();

			// Delay
			try 
			{
				Thread.sleep(20);
			} 
			catch (InterruptedException ex) 
			{
			}		
	}
			
}
}
}
