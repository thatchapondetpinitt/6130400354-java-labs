package detpinit.thatchapon.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.Random;

import detpinit.thatchapon.lab8.MyBall;
import detpinit.thatchapon.lab8.MyBrick;
import detpinit.thatchapon.lab8.MyCanvas;
import detpinit.thatchapon.lab8.MyPedal;

public class MyCanvasV8 extends MyCanvasV7 implements Runnable, KeyListener {
	protected int numCol = MyCanvas.WIDTH / MyBrick.brickWidth;
	protected int numRow = 7;
	protected int numVisibleBricks = numCol * numRow;
	protected MyBrickV2[][] bricks = new MyBrickV2[MyBrick.brickWidth][(super.HEIGHT / 3) - (MyBrick.brickHeight)];
	protected Thread running = new Thread(this);
	protected Color[] color = { Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.ORANGE,
			Color.RED };
	protected MyPedalV2 pedal = new MyPedalV2(MyCanvas.WIDTH / 2 - MyPedal.PedalWidth / 2,
			MyCanvas.HEIGHT - MyPedal.PedalHeight);
	protected int lives = 3;

	public MyCanvasV8() {
		// initialize bricks with numRow and numCol

		ball = new MyBallV2(MyCanvas.WIDTH / 2 - MyBall.DIAMETER / 2,
				MyCanvas.HEIGHT - MyBall.DIAMETER - MyPedal.PedalHeight);
		ball.ballVelX = 0;
		ball.ballVelY = 0;
		for (int i = 0; i < numBricks; i++) {
			for (int j = 0; j < numCol; j++) {
				bricks[i][j] = new MyBrickV2(j * MyBrick.brickWidth, (super.HEIGHT / 3) - (i * MyBrick.brickHeight));
			}
		}
		running.start();
		setFocusable(true);
		addKeyListener(this);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));

		for (int i = 0; i < numRow; i++) {
			for (int j = 0; j < numCol; j++) {
				if (bricks[i][j].visible) {
					g2d.setColor(color[i]);
					g2d.fill(bricks[i][j]);
					g2d.setColor(Color.BLACK);
					g2d.setStroke(new BasicStroke(4));
					g2d.draw(bricks[i][j]);
				}
			}
		}

		// Draw a ball
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);

		// Draw a pedal
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);

		// Draw score
		g2d.setFont(new Font("SanSerif", Font.BOLD, 20));
		g2d.setColor(Color.BLUE);
		String s = " Lives : " + lives;
		g2d.drawString(s, 10, 30);

		// Draw a string You won
		if (numVisibleBricks == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 72));
			g2d.setColor(Color.GREEN);
			String won = "You won";
			g2d.drawString(won, MyCanvas.WIDTH / 3, MyCanvas.HEIGHT / 2);
		}

		// Draw a string GAME OVER
		if (lives == 0) {
			g2d.setFont(new Font("SanSerif", Font.BOLD, 72));
			g2d.setColor(Color.GRAY);
			String won = "GAME OVER";
			g2d.drawString(won, MyCanvas.WIDTH / 4, MyCanvas.HEIGHT / 2);
		}
	}

	@Override
	public void run() {
		while (true) {
			// bounce the ball when hit the right or the left side of the wall
			if (ball.x + MyBall.DIAMETER >= MyCanvas.WIDTH) {
				ball.x = MyCanvas.WIDTH - MyBall.DIAMETER;
				ball.ballVelX *= -1;
			} else if (ball.x + MyBall.DIAMETER <= 30) {
				ball.x = 0;
				ball.ballVelX *= -1;
			}

			// bounce the ball when hit the top or the bottom of the wall
			else if (ball.y + MyBall.DIAMETER >= MyCanvas.HEIGHT) {
				ball.y = MyCanvas.HEIGHT - MyBall.DIAMETER;
				ball.ballVelY *= -1;
			} else if (ball.y + MyBall.DIAMETER <= 30) {
				ball.y = 0;
				ball.ballVelY *= -1;
			}

			// check if ball collides with bricks
			for (int i = 0; i < numRow; i++) {
				for (int j = 0; j < numCol; j++) {
					if (bricks[i][j].visible) {
						checkCollision(ball, bricks[i][j]);
					}
				}
			}

			// check if ball hit pedal
			collideWithPedal(ball, pedal);

			// check if ball pass the bottom
			checkPassBottom();

			// move the ball
			ball.move();
			repaint();

			// Delay
			try {
				Thread.sleep(20);
			} catch (InterruptedException ex) {

			}
		}
	}

	private void checkPassBottom() {
		if (ball.y + MyBall.DIAMETER >= MyCanvas.HEIGHT) {
			ball.x = pedal.x + MyPedal.PedalWidth / 2 - MyBall.DIAMETER / 2;
			ball.y = MyCanvas.HEIGHT - MyBall.DIAMETER - MyPedal.PedalHeight;
			ball.ballVelX = 0;
			ball.ballVelY = 0;
			lives--;
			repaint();
		}
	}

	public void checkCollision(MyBallV2 ball, MyBrickV2 brick) {
		double x = ball.x + MyBall.DIAMETER / 2.0;
		double y = ball.y + MyBall.DIAMETER / 2.0;
		double deltaX = x - Math.max(brick.x, Math.min(x, brick.x + MyBrick.brickWidth));
		double deltaY = y - Math.max(brick.y, Math.min(y, brick.y + MyBrick.brickHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.DIAMETER * MyBall.DIAMETER) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) { // collides top or bottom
				// Change the direction on y coordinate to the opposite
				ball.ballVelY *= -1;

			} else { // collides left or right
				// Change the direction on x coordinate to the opposite
				ball.ballVelX *= -1;

			}
			numVisibleBricks--;
			ball.move();
			brick.visible = false;
		}
	}

	private void collideWithPedal(MyBallV2 ball, MyPedalV2 pedal) {
		double x = ball.x + MyBall.DIAMETER / 2.0;
		double y = ball.y + MyBall.DIAMETER / 2.0;
		double deltaX = x - Math.max(pedal.x, Math.min(x, pedal.x + MyPedal.PedalWidth));
		double deltaY = y - Math.max(pedal.y, Math.min(y, pedal.y + MyPedal.PedalHeight));

		boolean collided = (deltaX * deltaX + deltaY * deltaY) < (MyBall.DIAMETER * MyBall.DIAMETER) / 4.0;

		if (collided) {
			if (deltaX * deltaX < deltaY * deltaY) {
				ball.ballVelY *= -1;

			} else {
				ball.ballVelX *= -1;

			}
			ball.move();
		}
	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT) { // check if press left arrow
			// move left
			pedal.moveLeft();

		} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) { // check if press right arrow
			// move right
			pedal.moveRight();

		} else if (e.getKeyCode() == KeyEvent.VK_SPACE) { // check if press spacebar
			// set ballVelY to 4
			ball.ballVelY = 4;
			// then randomly BallVelX to 4 or -4
			Random rand = new Random();
			int n = rand.nextInt(2);
			if (n == 0) {
				ball.ballVelX = 4;
			}
			if (n == 1) {
				ball.ballVelX = -4;
			}
		}

	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}
}