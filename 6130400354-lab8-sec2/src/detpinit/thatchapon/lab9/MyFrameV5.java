package detpinit.thatchapon.lab9;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.SwingUtilities;

import detpinit.thatchapon.lab8.MyFrameV4;

public class MyFrameV5 extends MyFrameV4 {
	public MyFrameV5(String text) {
		super(text);
	}

	public static void createAndShowGUI() {
		MyFrameV5 msw = new MyFrameV5("My Frame V5");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		add(new MyCanvasV5());
	}

}